![Logo](./docs/images/paperfigure_v6.png)
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/GPL-3.0)

# PyJAMAS

[**Py**JAMAS](https://bitbucket.org/rfg_lab/pyjamas/src/master/) is **J**ust **A** **M**ore **A**wesome **S**iesta.

## Documentation and installation instructions

You can find the official [PyJAMAS](https://bitbucket.org/rfg_lab/pyjamas/src/master/) documentation, with detailed
installation instructions, [**here**](https://pyjamas.readthedocs.io).

## Citing PyJAMAS

If you use [PyJAMAS](https://bitbucket.org/rfg_lab/pyjamas/src/master/), please cite:

Fernandez-Gonzalez R, Balaghi N, Wang K, Hawkins R, Rothenberg K, McFaul C, Schimmer C, Ly M, do Carmo A, Scepanovic G,
Erdemci-Tandogan G, Castle V. **PyJAMAS: open-source, multimodal segmentation and analysis of microscopy images**. *
Bioinformatics*. 2021 Aug 13:btab589. doi: 10.1093/bioinformatics/btab589.

## Sponsors

We are grateful for the generous support from the following agencies and institutions, which contribute to the
development and maintenance of [PyJAMAS](https://bitbucket.org/rfg_lab/pyjamas/src/master/):

![Sponsors](./docs/images/sponsors.png)
