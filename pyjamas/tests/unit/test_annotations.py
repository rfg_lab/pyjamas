"""
    PyJAMAS is Just A More Awesome Siesta
    Copyright (C) 2018  Rodrigo Fernandez-Gonzalez (rodrigo.fernandez.gonzalez@utoronto.ca)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# tests for image menu
# for coverage, run:
# coverage run -m pytest -s
# or if you want to include branches:
# coverage run --branch -m pytest
# followed by:
# coverage report -i

# without these two instructions, pytest seg-faults with "Garbage-collecting" as the error message when running test_cbBatchMeasure
import gc
gc.disable()

import os

import pytest

from pyjamas.pjscore import PyJAMAS
import pyjamas.tests.unit.pjsfixtures as pjsfixtures

PyJAMAS_FIXTURE: PyJAMAS = PyJAMAS()
os.makedirs(pjsfixtures.TMP_DIR, exist_ok=True)
PyJAMAS_FIXTURE.options.cbSetCWD(pjsfixtures.TMP_DIR)

@pytest.mark.usefixtures("image_fixture")
@pytest.mark.usefixtures("pjsannotationspath_fixture")
def test_cbDeleteAllFiducials(image_fixture, pjsannotationspath_fixture):
    PyJAMAS_FIXTURE.io.cbLoadArray(image_fixture)  # loads a sample image
    PyJAMAS_FIXTURE.io.cbLoadAnnotations([pjsannotationspath_fixture])  # loads sample annotations

    assert len(PyJAMAS_FIXTURE.fiducials[0]) > 0  # checking that fiducials are present in the first time point after loading annotation fixture
    assert len(PyJAMAS_FIXTURE.polylines[0]) > 0  # checking that polylines are present in the first time point
    assert PyJAMAS_FIXTURE.annotations.cbDeleteAllFiducials()  # checking that the function runs and returns True (success!!)
    assert len(PyJAMAS_FIXTURE.fiducials[0]) == 0  # checking that fiducials have been deleted in the first time point
    assert len(PyJAMAS_FIXTURE.fiducials[-1]) == 0  # and in the last
    assert len(PyJAMAS_FIXTURE.polylines[0]) > 0  # but polylines are still there
